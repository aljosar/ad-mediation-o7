# Ad Mediation by Aljoša Rakita

To run application locally on localhost:8080 run
```
 mvn spring-boot:run
```

## Usage

* Retrieve list of ad networks (sorted ascending based on metadata): __GET on /__
* Update list of ad networks: __POST on /__ with body being JSON array of object to be added with sdk and metadata properties. example body: [{"sdk": "Adx", "metadata": "1"}, {"sdk": "AdMob", "metadata": "2"}, {"sdk": "Unity Ads,", "metadata": "3"}]

* Delete list of ad networks: __DELETE on /__
* et ad network based on id: __GET on /ads/id__
* Update/add one ad network: __POST on /ads__ with body being JSON object of ad network. example body: {"sdk": "Unity Ads,", "metadata": "3"}
* Delete one ad network based on id: __DELETE on /ads/id__

## Deploying
Google cloud project must be created to deploy app to app-engine, bellow PROJECT_ID and REGION_ID are variables based on gcloud configuration. 
There are two enviroment variables to optionally set up at production: MONGO_DB (mongodb database) and MONGO_URI (mongodb connection uri)

```bash
 mvn clean package appengine:deploy -Dapp.deploy.projectId=PROJECT_ID
```

To view your app, use command:
```
gcloud app browse
```
Or navigate to `https://<PROJECT_ID>.<REGION_ID>.r.appspot.com`.

## Production improvement/changes

* Set up proppper CORS on app and whitelist ip on database
* Add some kind of authorization middleware