package com.admediation.admediation;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface AdNetworkRepository extends MongoRepository<AdNetwork, String> {

  public AdNetwork findBySdk(String sdk);
}