package com.admediation.admediation;

import java.io.*; 
import java.util.*; 
import java.net.URISyntaxException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.beans.factory.annotation.Autowired;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Update.update;
import static org.springframework.data.mongodb.core.query.Query.query;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;

@SpringBootApplication
@RestController
public class AdMediationApplication {

  @Autowired
  private AdNetworkRepository repository;

  public static void main(String[] args) {
    SpringApplication.run(AdMediationApplication.class, args);
  }

  @GetMapping("/")
  public ResponseEntity<?> get() throws Exception {
    return ResponseEntity.ok().body(repository.findAll(Sort.by("metadata").ascending()));
  }

  @GetMapping("/ads/{id}")
  public ResponseEntity<?> getOne(@PathVariable String id) throws URISyntaxException {
    return ResponseEntity.ok().body(repository.findById(id));
  }

  @CrossOrigin(origins = "http://localhost:9000")
  @PostMapping("/")
  public ResponseEntity<?> update(@RequestBody List<AdNetwork> ads) throws URISyntaxException {
    for (AdNetwork ad : ads) {
      repository.save(ad);
		}
    return ResponseEntity.accepted().body(ads);
  }

  @CrossOrigin(origins = "http://localhost:9000")
  @PostMapping("/ads")
  public ResponseEntity<?> updateOne(@RequestBody AdNetwork ad) throws URISyntaxException {
    repository.save(ad);
    return ResponseEntity.accepted().body(ad);
  }

  @CrossOrigin(origins = "http://localhost:9000")
  @DeleteMapping("/")
  public ResponseEntity<?> delete() throws Exception {
    repository.deleteAll();
    return ResponseEntity.noContent().build();
  }

  @CrossOrigin(origins = "http://localhost:9000")
  @DeleteMapping("/ads/{id}")
  public ResponseEntity<?> deleteOne(@PathVariable String id) throws URISyntaxException {
    repository.deleteById(id);
    return ResponseEntity.noContent().build();
  }
}
