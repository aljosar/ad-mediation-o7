package com.admediation.admediation;

import org.springframework.data.annotation.Id;

public class AdNetwork {

  @Id
  public String id;

  public String sdk;
  public String metadata;

  AdNetwork() {}

  AdNetwork(String sdk, String metadata) {
    this.sdk = sdk;
    this.metadata = metadata;
  }
}